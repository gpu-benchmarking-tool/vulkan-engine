cd src
rmdir /s /Q libs
cd ..

git submodule update --init --recursive
git submodule update --recursive --remote

cd libs\profiler
call build.bat
cd ..\..\..

cd src
mkdir libs

copy ..\libs\profiler\src\*.h* libs
copy ..\libs\profiler\build\src\libprofiler-library.a libs