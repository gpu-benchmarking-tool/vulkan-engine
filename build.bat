rmdir /s /Q build
mkdir build
cd build
qmake ../Vulkan-Engine.pro -platform win32-g++
mingw32-make
cd ..

rmdir /s /Q bin
mkdir bin
copy src\*.h* bin
cd bin
mkdir libs
copy ..\src\libs\* libs
cd ..
copy build\release\libVulkan-Engine.a bin