rm -rf build
mkdir build
cd build
qmake ../Vulkan-Engine.pro
make
cd ..

rm -rf bin
mkdir bin
cp build/libVulkan-Engine.a bin
cp src/*.h* bin
cp src/libs bin -r
