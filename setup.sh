rm -rf src/libs

git submodule update --init --recursive
git submodule update --recursive --remote

cd libs/profiler
./build.sh
cd ../..

cd src
mkdir libs

cp ../libs/profiler/src/*.h* libs
cp ../libs/profiler/bin/libprofiler-library.a libs