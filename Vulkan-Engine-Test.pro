#-------------------------------------------------
#
# Project created by QtCreator 2019-06-24T19:28:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Vulkan-Engine
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -Wno-old-style-cast
QMAKE_CXXFLAGS += -Wzero-as-null-pointer-constant

CONFIG += c++11

SOURCES += \
        src/main.cpp \
        src/mesh.cpp \
        src/setvkinfo.cpp \
        src/vulkanwindow.cpp \
        src/renderer.cpp \
        src/shader.cpp

HEADERS += \
        src/libs/GLInfo.hpp \
        src/libs/Profiler.h \
        src/mesh.h \
        src/vulkanwindow.h \
        src/renderer.h \
        src/shader.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    vulkanresources.qrc

# PROFILER LIBRARY
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/ -lprofiler-library
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/ -lprofiler-library
else:unix: LIBS += -L$$PWD/src/libs/ -lprofiler-library

INCLUDEPATH += $$PWD/src/libs
DEPENDPATH += $$PWD/src/libs

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/profiler-library.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/profiler-library.lib
else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
