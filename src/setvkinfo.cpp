/*__________________________________________________
 |                                                  |
 |    File: setvkinfo.cpp                           |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Used to set the vulkan           |
 |    information.                                  |
 |_________________________________________________*/



#include "renderer.h"


// Sets vulkan information.
void Renderer::setVkInfo(const VkPhysicalDeviceLimits *physicalDeviceLimits) {

    // Get device properties
    VkPhysicalDeviceProperties properties;
    window->vulkanInstance()->functions()->vkGetPhysicalDeviceProperties(window->physicalDevice(), &properties);
    profiler->setVkDeviceName(properties.deviceName);
    profiler->setVkApiVersion(properties.apiVersion);
    profiler->setVkDriverVersion(properties.driverVersion);
    profiler->setVkVendorID(properties.vendorID);
    profiler->setVkDeviceID(properties.deviceID);


    // Get extensions
    auto extensions = window->supportedDeviceExtensions();
    std::vector<std::string> vkExtensions;
    for (int i = 0; i < extensions.size(); ++i)
        vkExtensions.push_back(extensions[i].name.toStdString());
    profiler->setVkExtensions(vkExtensions);


    // Get limits
    profiler->vkInfo.maxImageDimension1D = physicalDeviceLimits->maxImageDimension1D;
    profiler->vkInfo.maxImageDimension2D = physicalDeviceLimits->maxImageDimension2D;
    profiler->vkInfo.maxImageDimension3D = physicalDeviceLimits->maxImageDimension3D;
    profiler->vkInfo.maxImageDimensionCube = physicalDeviceLimits->maxImageDimensionCube;
    profiler->vkInfo.maxImageArrayLayers = physicalDeviceLimits->maxImageArrayLayers;
    profiler->vkInfo.maxTexelBufferElements = physicalDeviceLimits->maxTexelBufferElements;
    profiler->vkInfo.maxUniformBufferRange = physicalDeviceLimits->maxUniformBufferRange;
    profiler->vkInfo.maxStorageBufferRange = physicalDeviceLimits->maxStorageBufferRange;
    profiler->vkInfo.maxPushConstantsSize = physicalDeviceLimits->maxPushConstantsSize;
    profiler->vkInfo.maxMemoryAllocationCount = physicalDeviceLimits->maxMemoryAllocationCount;
    profiler->vkInfo.maxSamplerAllocationCount = physicalDeviceLimits->maxSamplerAllocationCount;
    profiler->vkInfo.bufferImageGranularity = physicalDeviceLimits->bufferImageGranularity;
    profiler->vkInfo.sparseAddressSpaceSize = physicalDeviceLimits->sparseAddressSpaceSize;
    profiler->vkInfo.maxBoundDescriptorSets = physicalDeviceLimits->maxBoundDescriptorSets;
    profiler->vkInfo.maxPerStageDescriptorSamplers = physicalDeviceLimits->maxPerStageDescriptorSamplers;
    profiler->vkInfo.maxPerStageDescriptorUniformBuffers = physicalDeviceLimits->maxPerStageDescriptorUniformBuffers;
    profiler->vkInfo.maxPerStageDescriptorStorageBuffers = physicalDeviceLimits->maxPerStageDescriptorStorageBuffers;
    profiler->vkInfo.maxPerStageDescriptorSampledImages = physicalDeviceLimits->maxPerStageDescriptorSampledImages;
    profiler->vkInfo.maxPerStageDescriptorStorageImages = physicalDeviceLimits->maxPerStageDescriptorStorageImages;
    profiler->vkInfo.maxPerStageDescriptorInputAttachments = physicalDeviceLimits->maxPerStageDescriptorInputAttachments;
    profiler->vkInfo.maxPerStageResources = physicalDeviceLimits->maxPerStageResources;
    profiler->vkInfo.maxDescriptorSetSamplers = physicalDeviceLimits->maxDescriptorSetSamplers;
    profiler->vkInfo.maxDescriptorSetUniformBuffers = physicalDeviceLimits->maxDescriptorSetUniformBuffers;
    profiler->vkInfo.maxDescriptorSetUniformBuffersDynamic = physicalDeviceLimits->maxDescriptorSetUniformBuffersDynamic;
    profiler->vkInfo.maxDescriptorSetStorageBuffers = physicalDeviceLimits->maxDescriptorSetStorageBuffers;
    profiler->vkInfo.maxDescriptorSetStorageBuffersDynamic = physicalDeviceLimits->maxDescriptorSetStorageBuffersDynamic;
    profiler->vkInfo.maxDescriptorSetSampledImages = physicalDeviceLimits->maxDescriptorSetSampledImages;
    profiler->vkInfo.maxDescriptorSetStorageImages = physicalDeviceLimits->maxDescriptorSetStorageImages;
    profiler->vkInfo.maxDescriptorSetInputAttachments = physicalDeviceLimits->maxDescriptorSetInputAttachments;
    profiler->vkInfo.maxVertexInputAttributes = physicalDeviceLimits->maxVertexInputAttributes;
    profiler->vkInfo.maxVertexInputBindings = physicalDeviceLimits->maxVertexInputBindings;
    profiler->vkInfo.maxVertexInputAttributeOffset = physicalDeviceLimits->maxVertexInputAttributeOffset;
    profiler->vkInfo.maxVertexInputBindingStride = physicalDeviceLimits->maxVertexInputBindingStride;
    profiler->vkInfo.maxVertexOutputComponents = physicalDeviceLimits->maxVertexOutputComponents;
    profiler->vkInfo.maxTessellationGenerationLevel = physicalDeviceLimits->maxTessellationGenerationLevel;
    profiler->vkInfo.maxTessellationPatchSize = physicalDeviceLimits->maxTessellationPatchSize;
    profiler->vkInfo.maxTessellationControlPerVertexInputComponents = physicalDeviceLimits->maxTessellationControlPerVertexInputComponents;
    profiler->vkInfo.maxTessellationControlPerVertexOutputComponents = physicalDeviceLimits->maxTessellationControlPerVertexOutputComponents;
    profiler->vkInfo.maxTessellationControlPerPatchOutputComponents = physicalDeviceLimits->maxTessellationControlPerPatchOutputComponents;
    profiler->vkInfo.maxTessellationControlTotalOutputComponents = physicalDeviceLimits->maxTessellationControlTotalOutputComponents;
    profiler->vkInfo.maxTessellationEvaluationInputComponents = physicalDeviceLimits->maxTessellationEvaluationInputComponents;
    profiler->vkInfo.maxTessellationEvaluationOutputComponents = physicalDeviceLimits->maxTessellationEvaluationOutputComponents;
    profiler->vkInfo.maxGeometryShaderInvocations = physicalDeviceLimits->maxGeometryShaderInvocations;
    profiler->vkInfo.maxGeometryInputComponents = physicalDeviceLimits->maxGeometryInputComponents;
    profiler->vkInfo.maxGeometryOutputComponents = physicalDeviceLimits->maxGeometryOutputComponents;
    profiler->vkInfo.maxGeometryOutputVertices = physicalDeviceLimits->maxGeometryOutputVertices;
    profiler->vkInfo.maxGeometryTotalOutputComponents = physicalDeviceLimits->maxGeometryTotalOutputComponents;
    profiler->vkInfo.maxFragmentInputComponents = physicalDeviceLimits->maxFragmentInputComponents;
    profiler->vkInfo.maxFragmentOutputAttachments = physicalDeviceLimits->maxFragmentOutputAttachments;
    profiler->vkInfo.maxFragmentDualSrcAttachments = physicalDeviceLimits->maxFragmentDualSrcAttachments;
    profiler->vkInfo.maxFragmentCombinedOutputResources = physicalDeviceLimits->maxFragmentCombinedOutputResources;
    profiler->vkInfo.maxComputeSharedMemorySize = physicalDeviceLimits->maxComputeSharedMemorySize;
    profiler->vkInfo.maxComputeWorkGroupCountX = physicalDeviceLimits->maxComputeWorkGroupCount[0];
    profiler->vkInfo.maxComputeWorkGroupCountY = physicalDeviceLimits->maxComputeWorkGroupCount[1];
    profiler->vkInfo.maxComputeWorkGroupCountZ = physicalDeviceLimits->maxComputeWorkGroupCount[2];
    profiler->vkInfo.maxComputeWorkGroupInvocations = physicalDeviceLimits->maxComputeWorkGroupInvocations;
    profiler->vkInfo.maxComputeWorkGroupSizeX = physicalDeviceLimits->maxComputeWorkGroupSize[0];
    profiler->vkInfo.maxComputeWorkGroupSizeY = physicalDeviceLimits->maxComputeWorkGroupSize[1];
    profiler->vkInfo.maxComputeWorkGroupSizeZ = physicalDeviceLimits->maxComputeWorkGroupSize[2];
    profiler->vkInfo.subPixelPrecisionBits = physicalDeviceLimits->subPixelPrecisionBits;
    profiler->vkInfo.subTexelPrecisionBits = physicalDeviceLimits->subTexelPrecisionBits;
    profiler->vkInfo.mipmapPrecisionBits = physicalDeviceLimits->mipmapPrecisionBits;
    profiler->vkInfo.maxDrawIndexedIndexValue = physicalDeviceLimits->maxDrawIndexedIndexValue;
    profiler->vkInfo.maxDrawIndirectCount = physicalDeviceLimits->maxDrawIndirectCount;
    profiler->vkInfo.maxSamplerLodBias = physicalDeviceLimits->maxSamplerLodBias;
    profiler->vkInfo.maxSamplerAnisotropy = physicalDeviceLimits->maxSamplerAnisotropy;
    profiler->vkInfo.maxViewports = physicalDeviceLimits->maxViewports;
    profiler->vkInfo.maxViewportDimensionsX = physicalDeviceLimits->maxViewportDimensions[0];
    profiler->vkInfo.maxViewportDimensionsY = physicalDeviceLimits->maxViewportDimensions[1];
    profiler->vkInfo.viewportBoundsRangeX = physicalDeviceLimits->viewportBoundsRange[0];
    profiler->vkInfo.viewportBoundsRangeY = physicalDeviceLimits->viewportBoundsRange[1];
    profiler->vkInfo.viewportSubPixelBits = physicalDeviceLimits->viewportSubPixelBits;
    profiler->vkInfo.minMemoryMapAlignment = physicalDeviceLimits->minMemoryMapAlignment;
    profiler->vkInfo.minTexelBufferOffsetAlignment = physicalDeviceLimits->minTexelBufferOffsetAlignment;
    profiler->vkInfo.minUniformBufferOffsetAlignment = physicalDeviceLimits->minUniformBufferOffsetAlignment;
    profiler->vkInfo.minStorageBufferOffsetAlignment = physicalDeviceLimits->minStorageBufferOffsetAlignment;
    profiler->vkInfo.minTexelOffset = physicalDeviceLimits->minTexelOffset;
    profiler->vkInfo.maxTexelOffset = physicalDeviceLimits->maxTexelOffset;
    profiler->vkInfo.minTexelGatherOffset = physicalDeviceLimits->minTexelGatherOffset;
    profiler->vkInfo.maxTexelGatherOffset = physicalDeviceLimits->maxTexelGatherOffset;
    profiler->vkInfo.minInterpolationOffset = physicalDeviceLimits->minInterpolationOffset;
    profiler->vkInfo.maxInterpolationOffset = physicalDeviceLimits->maxInterpolationOffset;
    profiler->vkInfo.subPixelInterpolationOffsetBits = physicalDeviceLimits->subPixelInterpolationOffsetBits;
    profiler->vkInfo.maxFramebufferWidth = physicalDeviceLimits->maxFramebufferWidth;
    profiler->vkInfo.maxFramebufferHeight = physicalDeviceLimits->maxFramebufferHeight;
    profiler->vkInfo.maxFramebufferLayers = physicalDeviceLimits->maxFramebufferLayers;
    profiler->vkInfo.framebufferColorSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->framebufferColorSampleCounts));
    profiler->vkInfo.framebufferDepthSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->framebufferDepthSampleCounts));
    profiler->vkInfo.framebufferStencilSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->framebufferStencilSampleCounts));
    profiler->vkInfo.framebufferNoAttachmentsSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->framebufferNoAttachmentsSampleCounts));
    profiler->vkInfo.maxColorAttachments = physicalDeviceLimits->maxColorAttachments;
    profiler->vkInfo.sampledImageColorSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->sampledImageColorSampleCounts));
    profiler->vkInfo.sampledImageIntegerSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->sampledImageIntegerSampleCounts));
    profiler->vkInfo.sampledImageDepthSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->sampledImageDepthSampleCounts));
    profiler->vkInfo.sampledImageStencilSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->sampledImageStencilSampleCounts));
    profiler->vkInfo.storageImageSampleCounts = static_cast<gpu::Count>(static_cast<int>(physicalDeviceLimits->storageImageSampleCounts));
    profiler->vkInfo.maxSampleMaskWords = physicalDeviceLimits->maxSampleMaskWords;
    profiler->vkInfo.timestampComputeAndGraphics = physicalDeviceLimits->timestampComputeAndGraphics;
    profiler->vkInfo.timestampPeriod = physicalDeviceLimits->timestampPeriod;
    profiler->vkInfo.maxClipDistances = physicalDeviceLimits->maxClipDistances;
    profiler->vkInfo.maxCullDistances = physicalDeviceLimits->maxCullDistances;
    profiler->vkInfo.maxCombinedClipAndCullDistances = physicalDeviceLimits->maxCombinedClipAndCullDistances;
    profiler->vkInfo.discreteQueuePriorities = physicalDeviceLimits->discreteQueuePriorities;
    profiler->vkInfo.pointSizeRangeX = physicalDeviceLimits->pointSizeRange[0];
    profiler->vkInfo.pointSizeRangeY = physicalDeviceLimits->pointSizeRange[1];
    profiler->vkInfo.lineWidthRangeX = physicalDeviceLimits->lineWidthRange[0];
    profiler->vkInfo.lineWidthRangeY = physicalDeviceLimits->lineWidthRange[1];
    profiler->vkInfo.pointSizeGranularity = physicalDeviceLimits->pointSizeGranularity;
    profiler->vkInfo.lineWidthGranularity = physicalDeviceLimits->lineWidthGranularity;
    profiler->vkInfo.strictLines = physicalDeviceLimits->strictLines;
    profiler->vkInfo.standardSampleLocations = physicalDeviceLimits->standardSampleLocations;
    profiler->vkInfo.optimalBufferCopyOffsetAlignment = physicalDeviceLimits->optimalBufferCopyOffsetAlignment;
    profiler->vkInfo.optimalBufferCopyRowPitchAlignment = physicalDeviceLimits->optimalBufferCopyRowPitchAlignment;
    profiler->vkInfo.nonCoherentAtomSize = physicalDeviceLimits->nonCoherentAtomSize;
}
