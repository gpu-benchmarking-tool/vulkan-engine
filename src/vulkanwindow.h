/*__________________________________________________
 |                                                  |
 |    File: vulkanwindow.h                          |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Vulkan Window class.             |
 |_________________________________________________*/



#pragma once

#include "renderer.h"
#include <QLoggingCategory>
#include <QVulkanInstance>
#include "libs/Profiler.h"


/**
 * @brief      Class for vulkan window.
 * Vulkan Window class based on QVulkanWindow widget.
 */
class VulkanWindow : public QVulkanWindow {

  private:

    QVulkanInstance inst;
    gpu::Profiler *profiler;
    int targetFramerate;


  public:

    /**
     * @brief      Constructs the object.
     * The profiler is passed a a pointer so that the data can be retrieved by the GUI tool.
     *
     * @param      profiler  The profiler used to save the GPU information
     * @param      framerate The target framerate
     */
    VulkanWindow(gpu::Profiler *profiler = nullptr, int framerate = 60);


    /**
     * @brief      Creates the renderer.
     * QVulkanWindowRenderer contains all the buffers, data, resources, ... used to render the scene.
     *
     * @return     Vulkan renderer.
     */
    QVulkanWindowRenderer *createRenderer() override;
};
