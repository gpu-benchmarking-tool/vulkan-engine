/*__________________________________________________
 |                                                  |
 |    File: renderer.h                              |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Vulkan renderer.                 |
 |_________________________________________________*/



#pragma once

#include <QVulkanWindow>
#include <QVulkanFunctions>
#include <QVector>
#include <QSize>
#include <QTime>
#include <QElapsedTimer>

#include "shader.h"
#include "mesh.h"
#include "libs/Profiler.h"


/**
 * @brief      Class for renderer.
 * Renderer implementation based on the QVulkanWindowRenderer widget.
 */
class Renderer : public QVulkanWindowRenderer {

  public:

    /**
     * @brief      Constructs the object.
     * Creates an instance of the Renderer.
     *
     * @param      w         Vulkan Window (QVulkanWindow)
     * @param      profiler  The profiler used to record the GPU information
     * @param      framerate The target framerate (60 by default)
     */
    Renderer(QVulkanWindow *w, gpu::Profiler *profiler = nullptr, int framerate = 60);


    /**
     * @brief      Initialises the resources.
     * Initialises the shaders, models, ...
     */
    void initResources() override;


    /**
     * @brief      Initialises the swap chain resources.
     */
    void initSwapChainResources() override;


    /**
     * @brief      Releases the swap chain resources (cleans the swap chain).
     */
    void releaseSwapChainResources() override;


    /**
     * @brief      Releases the resources (clean).
     * Releases the shader resources, models data, ...
     */
    void releaseResources() override;


    /**
     * @brief      Starts the next frame.
     * Builds the next frame.
     */
    void startNextFrame() override;


  private:

    QVulkanWindow *window;
    QVulkanDeviceFunctions *devFuncs;
    QMatrix4x4 projectionMatrix;
    VkPipelineCache pipelineCache = nullptr;
    VkDescriptorPool descPool = nullptr;
    VkDescriptorSetLayout descSetLayout = nullptr;
    VkDescriptorSet descSet;
    VkPipelineLayout pipelineLayout = nullptr;
    VkPipeline pipeline = nullptr;
    Shader vertexShader, fragmentShader;
    VkDeviceSize vertexUniSize;
    VkDeviceSize fragmentUniSize;
    VkDeviceSize uniMemStartOffset;
    VkBuffer vertexBuffer = nullptr;
    VkBuffer uniformBuffer = nullptr;
    VkDeviceMemory bufferMemory = nullptr;
    std::vector<Mesh> meshes;
    VkBuffer instanceBuffer = nullptr;
    VkDeviceMemory instanceBufferMemory = nullptr;
    QByteArray instanceData;
    int gridSize;
    int instCount;
    int currentMesh;
    gpu::Profiler *profiler = nullptr;
    float rotation = 0.0f;
    QVector3D lightPosition;
    int targetFramerate;


    /**
     * @brief      Sets the vulkan information.
     *
     * @param[in]  physicalDeviceLimits  The physical device limits
     */
    void setVkInfo(const VkPhysicalDeviceLimits *physicalDeviceLimits);


    /**
     * @brief      Creates the pipeline.
     * Creates the graphics pipeline.
     */
    void createPipeline();


    /**
     * @brief      Creates the buffers.
     * Creates the vertex buffer, index buffer and the descriptors.
     */
    void createBuffers();


    /**
     * @brief      Creates the instance buffer.
     * Setups the scene.
     */
    void createInstanceBuffer();


    /**
     * @brief      Builds the frame.
     * Sets the render pass, the viewport and the scissors.
     */
    void buildFrame();


    /**
     * @brief      Builds draw calls for items.
     * Setups the draw calls for the scene items.
     */
    void buildDrawCallsForItems();


    /**
     * @brief      Alignes the memory.
     *
     * @param[in]  v          Device size
     * @param[in]  byteAlign  The byte align
     *
     * @return     The aligned size.
     */
    static inline VkDeviceSize aligned(VkDeviceSize v, VkDeviceSize byteAlign) {
        return (v + byteAlign - 1) & ~(byteAlign - 1);
    }


    /**
     * @brief      Gets the matrices.
     * Gets the MVP, model, model normal matrices and the eye position vector.
     *
     * @param      mvp          The mvp (Model View Projection) matrix
     * @param      model        The model matrix
     * @param      modelNormal  The model normal matrix
     * @param      eyePos       The eye position 3D vector
     */
    void getMatrices(QMatrix4x4 *mvp, QMatrix4x4 *model, QMatrix3x3 *modelNormal, QVector3D *eyePos);


    /**
     * @brief      Writes the fragment uniform.
     *
     * @param      p       Pointer to data
     * @param[in]  eyePos  The eye position
     */
    void writeFragUni(quint8 *p, const QVector3D &eyePos);
};
