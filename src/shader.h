/*__________________________________________________
 |                                                  |
 |        File: shader.cpp                          |
 |        Author: Nabil Sadeg                       |
 |                                                  |
 |        Description: Shader class.                |
 |_________________________________________________*/



#pragma once

#include <QVulkanInstance>


/**
 * @brief      Class for shader.
 * Loads a shader for file so that it can be used by the Vulkan Engine.
 */
class Shader {

  public:
    VkShaderModule shaderModule = nullptr;


    /**
     * @brief      Constructs the object.
     * Empty constructor.
     */
    Shader() {}


    /**
     * @brief      Loads a shader given a path.
     * Loads the shader from file and create the shader module using the
     * QVulkanInstance and the VkDevice.
     *
     * @param      instance    The vulkan instance
     * @param[in]  device      The logical device that interfaces with the physical GPU
     * @param[in]  shaderPath  The shader path
     *
     * @return     True if load is successful, False otherwise.
     */
    bool load(QVulkanInstance *instance, VkDevice device, const QString &shaderPath);


    /**
     * @brief      Determines if valid.
     * Checks if the shader has been loaded successfully.
     *
     * @return     True if valid, False otherwise.
     */
    bool isValid() { return shaderModule != nullptr; }
};

