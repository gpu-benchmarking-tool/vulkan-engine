/*__________________________________________________
 |                                                  |
 |    File: renderer.cpp                            |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Vulkan renderer.                 |
 |_________________________________________________*/



#include "renderer.h"



// Constructor.
Renderer::Renderer(QVulkanWindow *w, gpu::Profiler *profiler, int framerate) : window(w) {

    this->profiler = profiler;
    gridSize = 1;
    instCount = gridSize * gridSize;
    currentMesh = 0;
    targetFramerate = framerate;

    // Set the light
    lightPosition = QVector3D(0, 0, 25);

    // Load the meshes
    meshes.resize(7);
    meshes[0].load(QStringLiteral(":/assets/OBJ/cube0.buf"));
    meshes[1].load(QStringLiteral(":/assets/OBJ/cube1.buf"));
    meshes[2].load(QStringLiteral(":/assets/OBJ/cube2.buf"));
    meshes[3].load(QStringLiteral(":/assets/OBJ/cube3.buf"));
    meshes[4].load(QStringLiteral(":/assets/OBJ/cube4.buf"));
    meshes[5].load(QStringLiteral(":/assets/OBJ/cube5.buf"));
    meshes[6].load(QStringLiteral(":/assets/OBJ/cube6.buf"));
}


// Initialises the resources.
void Renderer::initResources() {

    // Get the Vulkan instance
    QVulkanInstance *instance = window->vulkanInstance();

    // Get the device
    VkDevice device = window->device();

    // Get the physical device limits
    const VkPhysicalDeviceLimits *physicalDeviceLimits = &window->physicalDeviceProperties()->limits;
    const VkDeviceSize uniformAlignement = physicalDeviceLimits->minUniformBufferOffsetAlignment;

    if (profiler) setVkInfo(physicalDeviceLimits);

    // Set the device functions
    devFuncs = instance->deviceFunctions(device);

    // mat3 is like 3 * vec3
    vertexUniSize = aligned(2 * 64 + 48, uniformAlignement);
    fragmentUniSize = aligned(6 * 16 + 12 + 2 * 4, uniformAlignement);

    // Load shaders
    vertexShader.load(instance, device, QStringLiteral(":/assets/Shaders/color_phong_vert.spv"));
    fragmentShader.load(instance, device, QStringLiteral(":/assets/Shaders/color_phong_frag.spv"));

    // Create the pipeline and the buffers
    createPipeline();
}


// Initialises the swap chain resources.
void Renderer::initSwapChainResources() {

    // Get the projection matrix
    projectionMatrix = window->clipCorrectionMatrix();

    // Get the swap chain image size
    const QSize size = window->swapChainImageSize();

    // Compute the projection matrix
    projectionMatrix.perspective(45.0f, size.width() / static_cast<float>(size.height()), .01f, 1000.0f);
}


// Cleans the swap chain resources.
void Renderer::releaseSwapChainResources() {}


// Cleans the resources.
void Renderer::releaseResources() {

    // Get the device
    VkDevice device = window->device();


    // Destroy descriptor
    if (descSetLayout) {
        devFuncs->vkDestroyDescriptorSetLayout(device, descSetLayout, nullptr);
        descSetLayout = nullptr;
    }

    if (descPool) {
        devFuncs->vkDestroyDescriptorPool(device, descPool, nullptr);
        descPool = nullptr;
    }


    // Destroy the pipeline
    if (pipeline) {
        devFuncs->vkDestroyPipeline(device, pipeline, nullptr);
        pipeline = nullptr;
    }

    if (pipelineLayout) {
        devFuncs->vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
        pipelineLayout = nullptr;
    }

    if (pipelineCache) {
        devFuncs->vkDestroyPipelineCache(device, pipelineCache, nullptr);
        pipelineCache = nullptr;
    }


    // Destroy the vertex buffer
    if (vertexBuffer) {
        devFuncs->vkDestroyBuffer(device, vertexBuffer, nullptr);
        vertexBuffer = nullptr;
    }


    // Destroy uniform buffer
    if (uniformBuffer) {
        devFuncs->vkDestroyBuffer(device, uniformBuffer, nullptr);
        uniformBuffer = nullptr;
    }


    // Destroy the instance data
    if (bufferMemory) {
        devFuncs->vkFreeMemory(device, bufferMemory, nullptr);
        bufferMemory = nullptr;
    }

    if (instanceBuffer) {
        devFuncs->vkDestroyBuffer(device, instanceBuffer, nullptr);
        instanceBuffer = nullptr;
    }

    if (instanceBufferMemory) {
        devFuncs->vkFreeMemory(device, instanceBufferMemory, nullptr);
        instanceBufferMemory = nullptr;
    }


    // Destroy shaders
    if (vertexShader.isValid())
        devFuncs->vkDestroyShaderModule(device, vertexShader.shaderModule, nullptr);

    if (fragmentShader.isValid())
        devFuncs->vkDestroyShaderModule(device, fragmentShader.shaderModule, nullptr);
}


// Creates the graphics pipeline.
void Renderer::createPipeline() {

    // Get the device
    VkDevice device = window->device();


    // Create the pipeline cache
    VkPipelineCacheCreateInfo pipelineCacheInfo;
    memset(&pipelineCacheInfo, 0, sizeof(pipelineCacheInfo));
    pipelineCacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
    VkResult error = devFuncs->vkCreatePipelineCache(device, &pipelineCacheInfo, nullptr, &pipelineCache);
    if (error != VK_SUCCESS) qFatal("Failed to create the pipeline cache: Error %d", error);


    // Create the vertex binding description
    VkVertexInputBindingDescription vertexBindingDesc[] = {
        {
            0, // binding
            8 * sizeof(float),
            VK_VERTEX_INPUT_RATE_VERTEX
        },
        {
            1,
            6 * sizeof(float),
            VK_VERTEX_INPUT_RATE_INSTANCE
        }
    };


    // Create the vertex attribute description
    VkVertexInputAttributeDescription vertexAttributeDesc[] = {
        {
            // position
            0, // location
            0, // binding
            VK_FORMAT_R32G32B32_SFLOAT,
            0 // offset
        },
        {
            // normal
            1,
            0,
            VK_FORMAT_R32G32B32_SFLOAT,
            5 * sizeof(float)
        },
        {
            // instTranslate
            2,
            1,
            VK_FORMAT_R32G32B32_SFLOAT,
            0
        },
        {
            // instDiffuseAdjust
            3,
            1,
            VK_FORMAT_R32G32B32_SFLOAT,
            3 * sizeof(float)
        }
    };


    // Create the vertex info
    VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.pNext = nullptr;
    vertexInputInfo.flags = 0;
    vertexInputInfo.vertexBindingDescriptionCount = sizeof(vertexBindingDesc) / sizeof(vertexBindingDesc[0]);;
    vertexInputInfo.pVertexBindingDescriptions = vertexBindingDesc;
    vertexInputInfo.vertexAttributeDescriptionCount = sizeof(vertexAttributeDesc) / sizeof(vertexAttributeDesc[0]);
    vertexInputInfo.pVertexAttributeDescriptions = vertexAttributeDesc;


    // Create the descriptor pool
    VkDescriptorPoolSize descPoolSize = {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 2 };
    VkDescriptorPoolCreateInfo descPoolInfo;
    memset(&descPoolInfo, 0, sizeof(descPoolInfo));
    descPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolInfo.maxSets = 1;
    descPoolInfo.poolSizeCount = 1;
    descPoolInfo.pPoolSizes = &descPoolSize;
    error = devFuncs->vkCreateDescriptorPool(device, &descPoolInfo, nullptr, &descPool);
    if (error != VK_SUCCESS) qFatal("Failed to create the descriptor pool: Error %d", error);


    // Create the descriptor set layout
    VkDescriptorSetLayoutBinding layoutBindings[] = {
        {
            0, // binding
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
            1, // descriptorCount
            VK_SHADER_STAGE_VERTEX_BIT,
            nullptr
        },
        {
            1,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
            1,
            VK_SHADER_STAGE_FRAGMENT_BIT,
            nullptr
        }
    };
    VkDescriptorSetLayoutCreateInfo descLayoutInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        nullptr,
        0,
        sizeof(layoutBindings) / sizeof(layoutBindings[0]),
        layoutBindings
    };
    error = devFuncs->vkCreateDescriptorSetLayout(device, &descLayoutInfo, nullptr, &descSetLayout);
    if (error != VK_SUCCESS) qFatal("Failed to create the descriptor set layout: Error %d", error);


    // Create the descriptor set
    VkDescriptorSetAllocateInfo descSetAllocInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        nullptr,
        descPool,
        1,
        &descSetLayout
    };
    error = devFuncs->vkAllocateDescriptorSets(device, &descSetAllocInfo, &descSet);
    if (error != VK_SUCCESS) qFatal("Failed to allocate the descriptor set: Error %d", error);


    // Create the pipeline layout
    VkPipelineLayoutCreateInfo pipelineLayoutInfo;
    memset(&pipelineLayoutInfo, 0, sizeof(pipelineLayoutInfo));
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descSetLayout;

    error = devFuncs->vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout);
    if (error != VK_SUCCESS) qFatal("Failed to create the pipeline layout: Error %d", error);


    // Create the pipeline info
    VkGraphicsPipelineCreateInfo pipelineInfo;
    memset(&pipelineInfo, 0, sizeof(pipelineInfo));
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;


    // Setup the shaders
    VkPipelineShaderStageCreateInfo shaderStages[2] = {
        {
            VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            nullptr,
            0,
            VK_SHADER_STAGE_VERTEX_BIT,
            vertexShader.shaderModule,
            "main",
            nullptr
        },
        {
            VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            nullptr,
            0,
            VK_SHADER_STAGE_FRAGMENT_BIT,
            fragmentShader.shaderModule,
            "main",
            nullptr
        }
    };
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;


    // Setup the input assembly
    VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    memset(&inputAssembly, 0, sizeof(inputAssembly));
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    pipelineInfo.pInputAssemblyState = &inputAssembly;


    // Setup the viewport
    VkPipelineViewportStateCreateInfo viewport;
    memset(&viewport, 0, sizeof(viewport));
    viewport.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport.viewportCount = 1;
    viewport.scissorCount = 1;
    pipelineInfo.pViewportState = &viewport;


    // Setup the rasterizer
    VkPipelineRasterizationStateCreateInfo rasterizer;
    memset(&rasterizer, 0, sizeof(rasterizer));
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.lineWidth = 1.0f;
    pipelineInfo.pRasterizationState = &rasterizer;


    // Setup the multi sampler
    VkPipelineMultisampleStateCreateInfo multisampler;
    memset(&multisampler, 0, sizeof(multisampler));
    multisampler.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampler.rasterizationSamples = window->sampleCountFlagBits();
    pipelineInfo.pMultisampleState = &multisampler;


    // Setup the depth stencil
    VkPipelineDepthStencilStateCreateInfo depthStencil;
    memset(&depthStencil, 0, sizeof(depthStencil));
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    pipelineInfo.pDepthStencilState = &depthStencil;


    // Setup the colour blend
    VkPipelineColorBlendStateCreateInfo colourBlend;
    memset(&colourBlend, 0, sizeof(colourBlend));
    colourBlend.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    VkPipelineColorBlendAttachmentState colourBlendAttachment;
    memset(&colourBlendAttachment, 0, sizeof(colourBlendAttachment));
    colourBlendAttachment.colorWriteMask = 0xF;
    colourBlend.attachmentCount = 1;
    colourBlend.pAttachments = &colourBlendAttachment;
    pipelineInfo.pColorBlendState = &colourBlend;


    // Setup the dynamic state
    VkDynamicState dynEnable[] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
    VkPipelineDynamicStateCreateInfo dynState;
    memset(&dynState, 0, sizeof(dynState));
    dynState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynState.dynamicStateCount = sizeof(dynEnable) / sizeof(VkDynamicState);
    dynState.pDynamicStates = dynEnable;
    pipelineInfo.pDynamicState = &dynState;


    // Create the graphics pipeline
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = window->defaultRenderPass();
    error = devFuncs->vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineInfo, nullptr, &pipeline);
    if (error != VK_SUCCESS) qFatal("Failed to create the graphics pipeline: Error %d", error);
}


// Creates the buffers.
void Renderer::createBuffers() {

    // If the vertex buffer is already set, we don't need to create it again
    if (vertexBuffer) return;


    // Get the device
    VkDevice device = window->device();


    // Get the number of concurrent frames
    const int concurrentFrameCount = window->concurrentFrameCount();


    // Create the vertex buffer
    VkBufferCreateInfo bufferInfo;
    memset(&bufferInfo, 0, sizeof(bufferInfo));
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    const int blockMeshByteCount = meshes[currentMesh].vertexCount * 8 * sizeof(float);
    bufferInfo.size = blockMeshByteCount;
    bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    VkResult error = devFuncs->vkCreateBuffer(device, &bufferInfo, nullptr, &vertexBuffer);
    if (error != VK_SUCCESS) qFatal("Failed to create the vertex buffer: Error %d", error);


    // Get the vertex memory requirements
    VkMemoryRequirements vertexMemoryRequirements;
    devFuncs->vkGetBufferMemoryRequirements(device, vertexBuffer, &vertexMemoryRequirements);


    // Create the uniform buffer
    bufferInfo.size = (vertexUniSize + fragmentUniSize) * concurrentFrameCount;
    bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    error = devFuncs->vkCreateBuffer(device, &bufferInfo, nullptr, &uniformBuffer);
    if (error != VK_SUCCESS) qFatal("Failed to create the uniform buffer: Error %d", error);


    // Get the uniform memory requirements
    VkMemoryRequirements uniMemoryRequirements;
    devFuncs->vkGetBufferMemoryRequirements(device, uniformBuffer, &uniMemoryRequirements);


    // Allocate memory
    uniMemStartOffset = aligned(0 + vertexMemoryRequirements.size, uniMemoryRequirements.alignment);
    VkMemoryAllocateInfo memAllocInfo = {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        nullptr,
        uniMemStartOffset + uniMemoryRequirements.size,
        window->hostVisibleMemoryIndex()
    };
    error = devFuncs->vkAllocateMemory(device, &memAllocInfo, nullptr, &bufferMemory);
    if (error != VK_SUCCESS) qFatal("Failed to allocate the memory: %d", error);


    // Bind the vertex memory buffer
    error = devFuncs->vkBindBufferMemory(device, vertexBuffer, bufferMemory, 0);
    if (error != VK_SUCCESS) qFatal("Failed to bind the vertex buffer memory: Error %d", error);


    // Bind the uniform buffer memory
    error = devFuncs->vkBindBufferMemory(device, uniformBuffer, bufferMemory, uniMemStartOffset);
    if (error != VK_SUCCESS) qFatal("Failed to bind the uniform buffer memory: Error %d", error);


    // Copy vertex data
    quint8 *p;
    error = devFuncs->vkMapMemory(device, bufferMemory, 0, uniMemStartOffset, 0, reinterpret_cast<void **>(&p));
    if (error != VK_SUCCESS) qFatal("Failed to map the memory: Error %d", error);

    memcpy(p, meshes[currentMesh].geom.constData(), blockMeshByteCount);
    devFuncs->vkUnmapMemory(device, bufferMemory);


    // Write descriptors for the uniform buffers in the vertex and fragment shaders
    VkDescriptorBufferInfo vertUni = { uniformBuffer, 0, vertexUniSize };
    VkDescriptorBufferInfo fragUni = { uniformBuffer, vertexUniSize, fragmentUniSize };

    VkWriteDescriptorSet descWrite[2];
    memset(descWrite, 0, sizeof(descWrite));
    descWrite[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descWrite[0].dstSet = descSet;
    descWrite[0].dstBinding = 0;
    descWrite[0].descriptorCount = 1;
    descWrite[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    descWrite[0].pBufferInfo = &vertUni;

    descWrite[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descWrite[1].dstSet = descSet;
    descWrite[1].dstBinding = 1;
    descWrite[1].descriptorCount = 1;
    descWrite[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    descWrite[1].pBufferInfo = &fragUni;


    // Update the descriptor sets
    devFuncs->vkUpdateDescriptorSets(device, 2, descWrite, 0, nullptr);
}


// Creates the instance buffer.
void Renderer::createInstanceBuffer() {

    // Compute the size per instance
    const VkDeviceSize PER_INSTANCE_DATA_SIZE = 6 * sizeof(float);


    // Get the device
    VkDevice device = window->device();


    // Allocate memory
    VkBufferCreateInfo bufferInfo;
    memset(&bufferInfo, 0, sizeof(bufferInfo));
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = instCount * PER_INSTANCE_DATA_SIZE;
    bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;


    // Create the instance buffer
    instanceData.resize(bufferInfo.size);

    VkResult error = devFuncs->vkCreateBuffer(device, &bufferInfo, nullptr, &instanceBuffer);
    if (error != VK_SUCCESS) qFatal("Failed to create the instance buffer: Error %d", error);


    // Set the memory requirements
    VkMemoryRequirements memoryRequirements;
    devFuncs->vkGetBufferMemoryRequirements(device, instanceBuffer, &memoryRequirements);


    VkMemoryAllocateInfo memAllocInfo = {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        nullptr,
        memoryRequirements.size,
        window->hostVisibleMemoryIndex()
    };
    error = devFuncs->vkAllocateMemory(device, &memAllocInfo, nullptr, &instanceBufferMemory);
    if (error != VK_SUCCESS) qFatal("Failed to allocate the memory: Error %d", error);

    error = devFuncs->vkBindBufferMemory(device, instanceBuffer, instanceBufferMemory, 0);
    if (error != VK_SUCCESS) qFatal("Failed to bind the instance buffer memory: Error %d", error);


    // Set the instances position and color
    char *p = instanceData.data();
    float offset = static_cast<float>(20.0 / gridSize);
    for (int i = 0; i < gridSize; ++i) {
        for (int j = 0; j < gridSize; ++j) {

            // Translation
            float t[] = { -10 + (offset * i), -10 + (offset * j), -20 };
            memcpy(p, t, 12);

            // Color
            float d[] = { static_cast<float>(i) / gridSize, 1 - static_cast<float>(i) / gridSize, 0 };
            memcpy(p + 12, d, 12);
            p += PER_INSTANCE_DATA_SIZE;
        }
    }


    // Map the memory
    quint8 *pp;
    error = devFuncs->vkMapMemory(device, instanceBufferMemory, 0, instCount * PER_INSTANCE_DATA_SIZE, 0,
                                  reinterpret_cast<void **>(&pp));
    if (error != VK_SUCCESS) qFatal("Failed to map the memory: Error %d", error);
    memcpy(pp, instanceData.constData(), instanceData.size());
    devFuncs->vkUnmapMemory(device, instanceBufferMemory);
}


// Builds the frame.
void Renderer::buildFrame() {

    // Create the buffers
    createBuffers();
    createInstanceBuffer();


    // Get the command buffer
    VkCommandBuffer commandBuffer = window->currentCommandBuffer();
    const QSize size = window->swapChainImageSize();


    // Set the clear values
    VkClearColorValue clearColour = {{ .0, .0, .0, 1.0f }};
    VkClearDepthStencilValue clearDepthStencil = { 1, 0 };
    VkClearValue clearValues[3];
    memset(clearValues, 0, sizeof(clearValues));
    clearValues[0].color = clearValues[2].color = clearColour;
    clearValues[1].depthStencil = clearDepthStencil;


    // Create the render pass
    VkRenderPassBeginInfo renderPassBeginInfo;
    memset(&renderPassBeginInfo, 0, sizeof(renderPassBeginInfo));
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = window->defaultRenderPass();
    renderPassBeginInfo.framebuffer = window->currentFramebuffer();
    renderPassBeginInfo.renderArea.extent.width = size.width();
    renderPassBeginInfo.renderArea.extent.height = size.height();
    renderPassBeginInfo.clearValueCount = window->sampleCountFlagBits() > VK_SAMPLE_COUNT_1_BIT ? 3 : 2;
    renderPassBeginInfo.pClearValues = clearValues;
    VkCommandBuffer cmdBuf = window->currentCommandBuffer();
    devFuncs->vkCmdBeginRenderPass(cmdBuf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);


    // Set the viewport
    VkViewport viewport = {
        0, 0,
        float(size.width()), float(size.height()),
        0, 1
    };
    devFuncs->vkCmdSetViewport(commandBuffer, 0, 1, &viewport);


    // Set the scissor
    VkRect2D scissor = {
        { 0, 0 },
        { uint32_t(size.width()), uint32_t(size.height()) }
    };
    devFuncs->vkCmdSetScissor(commandBuffer, 0, 1, &scissor);


    // Build draw calls for cubes
    buildDrawCallsForItems();


    // Finish the render pass
    devFuncs->vkCmdEndRenderPass(cmdBuf);
}


// Builds the draw calls for the items.
void Renderer::buildDrawCallsForItems() {

    // Get the device
    VkDevice device = window->device();

    VkCommandBuffer cb = window->currentCommandBuffer();


    // Bind the pipeline
    devFuncs->vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    VkDeviceSize vbOffset = 0;
    devFuncs->vkCmdBindVertexBuffers(cb, 0, 1, &vertexBuffer, &vbOffset);
    devFuncs->vkCmdBindVertexBuffers(cb, 1, 1, &instanceBuffer, &vbOffset);

    // Compute the offset
    uint32_t frameUniOffset = window->currentFrame() * (vertexUniSize + fragmentUniSize);
    uint32_t frameUniOffsets[] = { frameUniOffset, frameUniOffset };


    // Bind the descriptors
    devFuncs->vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
                                      &descSet, 2, frameUniOffsets);


    // Apply rotation
    rotation += 0.5;

    QMatrix4x4 vp, model;
    QMatrix3x3 modelNormal;
    QVector3D eyePos;
    getMatrices(&vp, &model, &modelNormal, &eyePos);

    // Map the uniform data for the current frame,
    quint8 *p;
    VkResult err = devFuncs->vkMapMemory(device, bufferMemory,
                                         uniMemStartOffset + frameUniOffset,
                                         vertexUniSize + fragmentUniSize,
                                         0, reinterpret_cast<void **>(&p));
    if (err != VK_SUCCESS)
        qFatal("Failed to map the memory: Error %d", err);


    // Vertex shader uniforms
    memcpy(p, vp.constData(), 64);
    memcpy(p + 64, model.constData(), 64);
    const float *mnp = modelNormal.constData();
    memcpy(p + 128, mnp, 12);
    memcpy(p + 128 + 16, mnp + 3, 12);
    memcpy(p + 128 + 32, mnp + 6, 12);


    // Fragment shader uniforms
    p += vertexUniSize;
    writeFragUni(p, eyePos);

    // Unmap the memory
    devFuncs->vkUnmapMemory(device, bufferMemory);


    // Draw
    devFuncs->vkCmdDraw(cb, meshes[currentMesh].vertexCount, instCount, 0, 0);
}


// Gets the matrices.
void Renderer::getMatrices(QMatrix4x4 *vp, QMatrix4x4 *model, QMatrix3x3 *modelNormal, QVector3D *eyePos) {

    // Set to identity
    model->setToIdentity();

    model->scale(20.0 / gridSize);

    // Apply rotation
    model->rotate(rotation, 0, 1, 0);

    // Compute the normal
    *modelNormal = model->normalMatrix();

    // Compute the viewport
    QMatrix4x4 view;
    *vp = projectionMatrix * view;

    // Compute the eye position
    *eyePos = view.inverted().column(3).toVector3D();
}


// Writes the fragment uniform buffer.
void Renderer::writeFragUni(quint8 *p, const QVector3D &eyePos) {

    // Camera position
    float ECCameraPosition[] = { eyePos.x(), eyePos.y(), eyePos.z() };
    memcpy(p, ECCameraPosition, 12);
    p += 16;

    // Material
    float ka[] = { 0.05f, 0.05f, 0.05f };
    memcpy(p, ka, 12);
    p += 16;

    float kd[] = { 0.7f, 0.7f, 0.7f };
    memcpy(p, kd, 12);
    p += 16;

    float ks[] = { 0.66f, 0.66f, 0.66f };
    memcpy(p, ks, 12);
    p += 16;

    // Light parameters
    float ECLightPosition[] = { lightPosition.x(), lightPosition.y(), lightPosition.z() };
    memcpy(p, ECLightPosition, 12);
    p += 16;

    // Attributes
    float attributes[] = { 1, 0, 0 };
    memcpy(p, attributes, 12);
    p += 16;

    // Color
    float color[] = { 1.0f, 1.0f, 1.0f };
    memcpy(p, color, 12);
    p += 12;

    // Intensity
    float intensity = 0.8f;
    memcpy(p, &intensity, 4);
    p += 4;

    // Specularity
    float specularExp = 150.0f;
    memcpy(p, &specularExp, 4);
    p += 4;
}


// Starts the next frame.
void Renderer::startNextFrame() {

    // Start the chrono and build the frame
    QElapsedTimer chrono;
    chrono.start();
    buildFrame();

    // Display the frame
    window->frameReady();
    window->requestUpdate();

    // Record elpased time
    qint64 elapsed =  chrono.elapsed();
    if (profiler) profiler->addTrianglesEntry(instCount * (meshes[currentMesh].vertexCount / 3), elapsed);

    // Stop if we don't do 60 FPS
    if (elapsed > 1000. / targetFramerate) window->close();

    // Increase poly count
    if (currentMesh < meshes.size()-1) ++currentMesh;
    else {
        ++gridSize;
        instCount = gridSize * gridSize;
    }
}
