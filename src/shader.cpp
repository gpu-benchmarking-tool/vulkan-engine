/*___________________________________________
 |                                           |
 | File: shader.cpp                          |
 | Author: Nabil Sadeg                       |
 |                                           |
 | Description: Shader class.                |
 |__________________________________________*/


#include "shader.h"
#include <QFile>
#include <QVulkanDeviceFunctions>


// Loads a shader.
bool Shader::load(QVulkanInstance *instance, VkDevice device, const QString &shaderPath) {

    // Open the file
    QFile file(shaderPath);
    if (!file.open(QIODevice::ReadOnly)) return false;

    // Read the data
    QByteArray blob = file.readAll();

    // Create the shader module info
    VkShaderModuleCreateInfo shaderInfo;
    memset(&shaderInfo, 0, sizeof(shaderInfo));
    shaderInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderInfo.codeSize = blob.size();
    shaderInfo.pCode = reinterpret_cast<const uint32_t *>(blob.constData());
    VkResult err = instance->deviceFunctions(device)->vkCreateShaderModule(device, &shaderInfo, nullptr, &shaderModule);
    if (err != VK_SUCCESS) return false;

    return true;
}
