/*__________________________________________________
 |													|
 |	File: main.cpp									|
 |	Author: Nabil Sadeg								|
 |													|
 |	Description: Main function.						|
 |_________________________________________________*/



#include <QGuiApplication>
#include "vulkanwindow.h"


// Main function.
int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    gpu::Profiler *p = new gpu::Profiler();
    VulkanWindow w(p);
    w.resize(1024, 768);
    w.show();

    return app.exec();
}
