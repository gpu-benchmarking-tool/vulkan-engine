/*__________________________________________________
 |                                                  |
 |    File: vulkanwindow.cpp                        |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Vulkan Window class.             |
 |_________________________________________________*/



#include "vulkanwindow.h"


// Set the logging category
Q_LOGGING_CATEGORY(lcVk, "qt.vulkan")


// Constructor.
VulkanWindow::VulkanWindow(gpu::Profiler *profiler, int framerate) {

    // Set the profiler
    this->profiler = profiler;

    // Set the target framerate
    targetFramerate = framerate;

    // Set the layers
    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

    inst.setLayers(QByteArrayList()
                   << "VK_LAYER_GOOGLE_threading"
                   << "VK_LAYER_LUNARG_parameter_validation"
                   << "VK_LAYER_LUNARG_object_tracker"
                   << "VK_LAYER_LUNARG_core_validation"
                   << "VK_LAYER_LUNARG_image"
                   << "VK_LAYER_LUNARG_swapchain"
                   << "VK_LAYER_GOOGLE_unique_objects");

    // Create the instance
    if (!inst.create())
        qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

    setVulkanInstance(&inst);
    setTitle("Vulkan Engine");
    setIcon(QIcon(":/assets/Icons/Vulkan.svg"));
}


// Creates the renderer.
QVulkanWindowRenderer *VulkanWindow::createRenderer() {
    return new Renderer(this, profiler, targetFramerate);
}
