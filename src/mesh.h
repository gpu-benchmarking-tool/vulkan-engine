/*__________________________________________________
 |                                                  |
 |    File: mesh.h                                  |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Mesh class.                      |
 |_________________________________________________*/



#pragma once

#include <QString>
#include <QFile>


/**
 * @brief      Class for mesh.
 * Loads the 3D model (mesh).
 * Keeps track of vertex count and saves data in a QByteArray.
 */
class Mesh {

  public:

    int vertexCount = 0;
    float aabb[6];
    QByteArray geom;


    /**
     * @brief      Constructs the object.
     * Empty constructor.
     */
    Mesh() {}


    /**
     * @brief      Loads mesh.
     * Loads mesh from file and saves data in QByteArray.
     *
     * @param[in]  file  The file to load
     *
     * @return     True if load is successful, False otherwise.
     */
    bool load(const QString &file);
};
