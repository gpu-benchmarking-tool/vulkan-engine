/*__________________________________________________
 |                                                  |
 |    File: mesh.cpp                                |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Mesh class.                      |
 |_________________________________________________*/



#include "mesh.h"


bool Mesh::load(const QString &file) {

    // Open the file
    QFile f(file);
    if (!f.open(QIODevice::ReadOnly)) return false;

    // Read the file
    QByteArray buf = f.readAll();
    const char *p = buf.constData();
    quint32 format;
    memcpy(&format, p, 4);

    // Check the format
    if (format != 1) return false;

    // Extract the data
    int ofs = 4;
    memcpy(&vertexCount, p + ofs, 4);
    ofs += 4;
    memcpy(aabb, p + ofs, 6 * 4);
    ofs += 6 * 4;
    const int byteCount = vertexCount * 8 * 4;
    geom.resize(byteCount);
    memcpy(geom.data(), p + ofs, byteCount);

    return true;
}
