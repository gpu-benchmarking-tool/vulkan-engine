git submodule update --init --recursive
git submodule update --remote --recursive
doxygen Doxygen
cd doc\latex
call make
cd ..\..
copy doc\latex\refman.pdf doc
cd doc
ren refman.pdf Vulkan-Engine.pdf